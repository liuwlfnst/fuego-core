#!/usr/bin/python
# gen-page.py - generate a test, testset or testcase page
#
# Todo:
#  - read template
#  - replace macro
#  - write rst file
#

import sys
import os

FUEGO_CORE=os.environ.get("FUEGO_CORE","/fuego-core")
FUEGO_RW=os.environ.get("FUEGO_RW","/fuego-rw")
outdir=FUEGO_RW+"/docs"

def usage(rcode):
    print("gen-page.py <filename-or-tguid>")
    sys.exit(rcode)

def error_out(msg):
    print(msg)
    sys.exit(1)

# If the tguid only has 3 parts,
# the 3rd part is the testset, not the testcase
# FIXTHIS - this doesn't handle multi-part testsets correctly
#  that is, if a test has:
#    test_name=Functional.longsets
#    test_set=foo.bar
#    test_case=baz
# there's no way to reference a template file for Functional.longsets.foo.bar
# (Fuego will think that bar is the testcase)
def split_tguid(tguid):
    parts = tguid.split(".")
    test_name=".".join(parts[0:2])
    del(parts[0])
    del(parts[0])
    if parts:
        test_set = parts[0]
        del parts[0]
        test_case=None
        if len(parts)>1:
            test_case = parts[-1]
        test_set += "." + ".".join(parts[:-1])
    else:
        test_set=None
        test_case=None
    return (test_name, test_set, test_case)

def file_from_path(path):
    basename = os.path.basename(path)
    if not basename.endswith(".ftmp"):
        error_out("Error: Invalid test template reference '%s'" % path)
    tguid = basename[:-5]
    if "." not in tguid:
        error_out("Error: Invalid test template reference '%s'" % tguid)
    test_name, test_set, test_case = split_tguid(tguid)
    return (path, test_name, test_set, test_case)


def file_from_tguid(tguid):
    if "." not in tguid:
        error_out("Error: Invalid test template reference '%s'" % tguid)
    test_name, test_set, test_case = split_tguid(tguid)
    filename = tguid + ".ftmp"
    filepath=FUEGO_CORE+"/engine/tests/" + test_name + "/docs" + filename
    return (filepath, test_name, test_set, test_case)

macros = [".. fuego_result_list::",
        ".. fuego_variant_list::",
        ".. fuego_status::",
        ".. fuego_result::",
         ]

def get_macro_value(macro, test_name, test_set, test_case):
    # FIXTHIS - run ftc or something to generate real data
    if macro==".. fuego_result_list::":
        return """Here is the result list for %s.%s.%s
 * foo
 * bar
 * baz

""" % (test_name, test_set, test_case)

    if macro==".. fuego_variant_list::":
        return """Here is the variant list for %s.%s.%s
 * default
 * shorter

""" % (test_name, test_set, test_case)

    if macro==".. fuego_status::":
        return """Status: OK

"""

    if macro==".. fuego_result::":
        return """Result: OK

"""
    msg = "ERROR: Unknown Macro '%s'" % macro
    print msg
    return msg

def main():
    if '-h' in sys.argv or '--help' in sys.argv:
        usage(0)

    # read template file
    template_ref=sys.argv[1]
    if os.path.exists(template_ref):
        filepath, test_name, test_set, test_case = file_from_path(template_ref)
    else:
        filepath, test_name, test_set, test_case = file_from_tguid(template_ref)

    if not os.path.exists(filepath):
        print("ERROR: Invalid template reference: %s" % template_ref)

    lines = open(filepath).readlines()

    new_lines = []
    for line in lines:
        found = False
        for macro in macros:
            if line.startswith(macro):
                found = True
                content = get_macro_value(macro, test_name, test_set, test_case)
                new_lines.append(content)
        if not found:
            new_lines.append(line)

    # print new_lines

    # write upated content to .rst file
    dirname = os.path.dirname(filepath)
    basename = os.path.basename(filepath)
    outpath = outdir + os.sep + basename[:-5]+".rst"
    print("output=%s" % outpath)
    fd = open(outpath,"w")
    fd.writelines(new_lines)
    fd.close()

main()
