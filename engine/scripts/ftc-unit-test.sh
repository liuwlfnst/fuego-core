#!/bin/bash
#
# this script performs a unit test of ftc
#
TEST_BOARD_FILE=/fuego-ro/boards/ftc-test.board
PATCH_FILE=add-ftc-test-node.patch

#set -x

##################################
# do some setup for the tests
#

#
# download clitest if it's not here,
#
if [ ! -f /usr/local/bin/clitest ] ; then
	curl -sOL https://raw.githubusercontent.com/aureliojargas/clitest/master/clitest
	chmod a+x clitest
	sudo mv clitest /usr/local/bin
fi

#
# create a test board file to work on:
#
cat <<'EOF' > $TEST_BOARD_FILE
inherit "base-board"
include "base-params"

IPADDR="192.168.7.2"
SSH_PORT="22"
LOGIN="root"
FUEGO_HOME="/home/a"
PASSWORD=""
PLATFORM="fake-platform"
TRANSPORT="ssh"
ARCHITECTURE="arm"

SATA_DEV="/dev/sdb1"
SATA_MP="/mnt/sata"

USB_DEV="/dev/sda1"
USB_MP="/mnt/usb"

MMC_DEV="/dev/mmcblk0p2"
MMC_MP="/mnt/mmc"

EXPAT_SUBTEST_COUNT_POS="1769"
EXPAT_SUBTEST_COUNT_NEG="41"

# now for some data to test ftc
# make all these appear at end of alphabetical list of vars
ZFOO="value1"

function zfoo() {
	echo "hello from zfoo"
	ls -l
}

function zbar() {
	echo "hello from zbar"
	echo "misplaced brace"
   }

# test some overrides
override ZFOO="value2 (overridden)"

override-func zfoo() {
	echo "hello from zfoo (overridden)"
	find . -type d
}

# the following should emit warnings
override ZZFOO="not here"

override-func zzbar() {
	echo "in zzbar override"
}
EOF

#
# create a slave node for this work:
#  1. create a patch file
#  2. back up config.xml
#  3. patch /userdata/conf/config.xml
#  4. remove patch file
#
cat <<'EOFP' > $PATCH_FILE
--- config.xml.old	2016-10-07 01:01:39.108275686 +0000
+++ config.xml.new	2016-10-07 01:01:17.332274941 +0000
@@ -172,6 +172,36 @@
       </nodeProperties>
       <userId>anonymous</userId>
     </slave>
+    <slave>
+      <name>ftc-test</name>
+      <description></description>
+      <remoteFS>/tmp/dev-slave1</remoteFS>
+      <numExecutors>1</numExecutors>
+      <mode>NORMAL</mode>
+      <retentionStrategy class="hudson.slaves.RetentionStrategy$Always"/>
+      <launcher class="hudson.slaves.CommandLauncher">
+        <agentCommand>java -jar /fuego-core/engine/slave.jar</agentCommand>
+      </launcher>
+      <label></label>
+      <userId>anonymous</userId>
+    </slave>
   </slaves>
   <quietPeriod>0</quietPeriod>
   <scmCheckoutRetryCount>0</scmCheckoutRetryCount>
EOFP

# this isn't racy at all...
cp /userdata/conf/config.xml /tmp/config.xml.backup
patch /userdata/conf/config.xml <$PATCH_FILE
rm $PATCH_FILE

# if I send commands to Jenkins, I should force it to re-read it's
# configuration here
# could use: 'jcmd reload-configuration'

##################################
# execute the tests
clitest --prefix tab ftc-tests

##################################
# clean up
rm $TEST_BOARD_FILE
cp /tmp/config.xml.backup /userdata/conf/config.xml
rm /tmp/config.xml.backup
