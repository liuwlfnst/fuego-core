tarball=glib2-test.tar.bz2

function test_build {

	cd ./glib-2.46.2

	if [ $PREFIX = "arm-agl-linux-gnueabi" ] ; then
		echo "This is porter!"
	else
		./configure --host=$PREFIX --disable-silent-rules --disable-dependency-tracking --enable-included-printf=no --disable-dtrace --disable-fam --disable-libelf --disable-systemtap --disable-man --disable-static --enable-nls --disable-gtk-doc --disable-gtk-doc-html --disable-gtk-doc-pdf --with-pcre=system --enable-always-build-tests 
		make
	fi
}

function test_deploy {
	if [ $PREFIX = "arm-agl-linux-gnueabi" ] ; then
		put -r ./glib-2.46.2/bin_porter/tests $BOARD_TESTDIR/fuego.$TESTDIR/
	else
		put -r ./glib-2.46.2/tests $BOARD_TESTDIR/fuego.$TESTDIR/
	fi	


}

function test_run {
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; export PATH=$BOARD_TESTDIR/fuego.$TESTDIR:$PATH; chmod -R 777 *; \
	cd tests/.libs; \
	cp * ..; \
	cd ..; \

	if ./asyncqueue-test; then echo 'TEST-1 OK'; else echo 'TEST-1 FAIL'; fi; \
	if ./atomic-test; then echo 'TEST-2 OK'; else echo 'TEST-2 FAIL'; fi; \
	if ./bit-test; then echo 'TEST-3 OK'; else echo 'TEST-3 FAIL'; fi; \
	if ./child-test; then echo 'TEST-4 OK'; else echo 'TEST-4 FAIL'; fi; \
	if ./completion-test; then echo 'TEST-5 OK'; else echo 'TEST-5 FAIL'; fi; \
	if ./datetime; then echo 'TEST-6 OK'; else echo 'TEST-6 FAIL'; fi; \
	if ./dirname-test; then echo 'TEST-7 OK'; else echo 'TEST-7 FAIL'; fi; \
	if ./env-test; then echo 'TEST-8 OK'; else echo 'TEST-8 FAIL'; fi; \
	if ./file-test; then echo 'TEST-9 OK'; else echo 'TEST-9 FAIL'; fi; \
	if ./gio-test; then echo 'TEST-10 OK'; else echo 'TEST-10 FAIL'; fi; \
	if ./mainloop-test; then echo 'TEST-11 OK'; else echo 'TEST-11 FAIL'; fi; \
	if ./mapping-test; then echo 'TEST-12 OK'; else echo 'TEST-12 FAIL'; fi; \
	export LD_LIBRARY_PATH=$BOARD_TESTDIR/fuego.$TESTDIR:$PATH/tests:$LD_LIBRARY_PATH; \
	if ./module-test; then echo 'TEST-13 OK'; else echo 'TEST-13 FAIL'; fi; \
	if ./onceinit; then echo 'TEST-14 OK'; else echo 'TEST-14 FAIL'; fi; \
	if ./qsort-test; then echo 'TEST-15 OK'; else echo 'TEST-15 FAIL'; fi; \
	if ./relation-test; then echo 'TEST-16 OK'; else echo 'TEST-16 FAIL'; fi; \
	if ./slice-color; then echo 'TEST-17 OK'; else echo 'TEST-17 FAIL'; fi; \
	if ./slice-concurrent; then echo 'TEST-18 OK'; else echo 'TEST-18 FAIL'; fi; \
	if ./slice-test; then echo 'TEST-19 OK'; else echo 'TEST-19 FAIL'; fi; \
	if ./slice-threadinit; then echo 'TEST-20 OK'; else echo 'TEST-20 FAIL'; fi; \
	if ./sources; then echo 'TEST-21 OK'; else echo 'TEST-21 FAIL'; fi; \
	if ./spawn-test; then echo 'TEST-22 OK'; else echo 'TEST-22 FAIL'; fi; \
	if ./testgdate; then echo 'TEST-23 OK'; else echo 'TEST-23 FAIL'; fi; \
	if ./testglib; then echo 'TEST-24 OK'; else echo 'TEST-24 FAIL'; fi; \
	if ./threadpool-test; then echo 'TEST-25 OK'; else echo 'TEST-25 FAIL'; fi; \
	if ./thread-test; then echo 'TEST-26 OK'; else echo 'TEST-26 FAIL'; fi; \
	if ./timeloop; then echo 'TEST-27 OK'; else echo 'TEST-27 FAIL'; fi; \
	if ./type-test; then echo 'TEST-28 OK'; else echo 'TEST-28 FAIL'; fi; \
	if ./unicode-caseconv; then echo 'TEST-29 OK'; else echo 'TEST-29 FAIL'; fi"
#	if ./iochannel-test; then echo 'TEST- OK'; else echo 'TEST- FAIL'; fi; \
#	if ./unicode-caseconv; then echo 'TEST- OK'; else echo 'TEST- FAIL'; fi; \
#	if ./unicode-collate; then echo 'TEST- OK'; else echo 'TEST- FAIL'; fi; \
#	if ./unicode-encoding; then echo 'TEST- OK'; else echo 'TEST- FAIL'; fi
}

function test_processing {
	P_CRIT="TEST.*"
	N_CRIT="TEST.*FAIL"

	log_compare "$TESTDIR" "29" "${P_CRIT}" "p"
	log_compare "$TESTDIR" "0" "${N_CRIT}" "n"
}




