# FIXTHIS: use https://github.com/linux-test-project/ltp.git instead of a tarball
tarball=ltp-7eb77fbfd80.tar.bz2

NEED_ROOT=1

ALLTESTS="
admin_tools         fs_perms_simple       ltp-aiodio.part3  net_stress.appl                   quickhit
can                 fs_readonly           ltp-aiodio.part4  net_stress.broken_ip              rpc_tests
cap_bounds          fsx                   ltplite           net_stress.interfaces             sched
commands            hugetlb               lvm.part1         net_stress.ipsec_icmp             scsi_debug.part1
connectors          hyperthreading        lvm.part2         net_stress.ipsec_tcp              securebits
containers          ima                   math              net_stress.ipsec_udp              smack
controllers         input                 mm                net_stress.multicast              stress.part1
cpuhotplug          io                    modules           net_stress.route                  stress.part2
crashme             io_cd                 net.features      network_commands                  stress.part3
dio                 io_floppy             net.ipv6          nptl                              syscalls
dma_thread_diotest  ipc                   net.ipv6_lib      numa                              syscalls-ipc
fcntl-locktests     kernel_misc           net.multicast     nw_under_ns                       timers
filecaps            ltp-aio-stress.part1  net.nfs           pipes                             tirpc_tests
fs                  ltp-aio-stress.part2  net.rpc           power_management_tests            tpm_tools
fs_bind             ltp-aiodio.part1      net.sctp          power_management_tests_exclusive  tracing
fs_ext4             ltp-aiodio.part2      net.tcp_cmds      pty"

ALLPTSTESTS="AIO MEM MSG SEM SIG THR TMR TPS"

ALLRTTESTS="
perf/latency
func/measurement
func/hrtimer-prio
func/gtod_latency
func/periodic_cpu_load
func/pthread_kill_latency
func/sched_football
func/pi-tests
func/thread_clock
func/rt-migrate
func/matrix_mult
func/prio-preempt
func/prio-wake
func/pi_perf
func/sched_latency
func/async_handler
func/sched_jitter
"

# OK - the logic here is a bit complicated
# We support several different usage scenarios:
#   1: fuego builds, deploys and installs LTP
#   2: fuego runs an existing installation of LTP
#   3: fuego builds and deploys LTP, to a special location
#      - this is a one-time operation
#      - this sets up the system for 2
#      - this is implemented by spec: "install"
#   4: fuego builds LTP, but deploy is left to user (manual)
#      - this creates a tarball that the user can deploy
#      - this is a one-time operation
#      - this set up the system for 2
#      - this is implemented by spec: "make_pkg"
#
# variables used:
# from board file or spec:
#   FUNCTIONAL_LTP_HOMEDIR = persistent location for LTP on board
#   FUNCTIONAL_LTP_PHASES = list of phases to force doing
#     - include string 'build' to do a build
#     - include string 'deploy' to do a deploy
#     - include string 'maketar' to do create a tarball for the target
#     - include string 'run' to do a run
#     - if empty, defaults to "build deploy run"
#
# scenario 1:
#  FUNCTIONAL_LTP_HOMEDIR="", FUNTIONAL_LTP_PHASES=""
# scenario 2:
#  FUNCTIONAL_LTP_HOMEDIR="/opt/ltp", FUNCTIONAL_LTP_PHASES=""
#  if not pre-installed, it's an error (detected in pre_check)
# scenario 3:
#  FUNCTIONAL_LTP_HOMEDIR="/opt/ltp", FUNCTIONAL_LTP_PHASES="build deploy"
#  if already pre-installed, it's an error (or rm -rf is done in deploy)
# scenario 4:
#  FUNCTIONAL_LTP_HOMEDIR="/opt/ltp", FUNCTIONAL_LTP_PHASES="build maketar"
#  user should copy tar to board, and install it, then set board file's
#    FUNCTIONAL_LTP_HOMEDIR var

# $1: string with tests to skip separated by spaces
# $2: absolute path to the skipfile (default: ${LOGDIR}/skiplist.txt)
function skip_tests {
    # split $1 on whitespace, without file globbing
    set -f
    local TESTS=($1)
    local SKIPFILE="${2:-${LOGDIR}/skiplist.txt}"
    set +f

    echo "Skipping tests: ${TESTS[@]}."
    for testname in "${TESTS[@]}"; do
        echo "$testname" >> ${SKIPFILE}
    done
}

# $1: command/program to look for
# $2: string with LTP test names separated by spaces
function skip_if_command_unavailable {
    local PROGRAM="$1"
    local TESTS="$2"
    export FOUND=""

    is_on_target ${PROGRAM} FOUND /bin:/usr/bin:/usr/sbin:/usr/local/bin
    if [ -z "${FOUND}" ]; then
        echo "WARNING: ${PROGRAM} is not installed on the target."
        skip_tests "${TESTS}"
    fi
}

# $1: a HAVE_xxx option to grep from ltp's include/config.h (after ./configure)
# $2: string with LTP test names separated by spaces
# $3: optional message
# [Note] can only be called after the configure step has finished
function skip_if_config_unavailable {
    local CONFIG="$1"
    local TESTS="$2"
    local MSG="$3"

    if ! grep "#define $CONFIG" ${WORKSPACE}/${JOB_BUILD_DIR}/include/config.h; then
        echo "WARNING: $CONFIG is not available."
        skip_tests "${TESTS}" "${WORKSPACE}/${JOB_BUILD_DIR}/buildskipfile.txt"
        if [ -n "$MSG" ]; then
            echo "$MSG"
        fi
    fi

}

# $1: string with kernel config options separated by spaces
# $2: string with LTP test names separated by spaces
function skip_if_kconfig_differs {
    set -f
    local KCONFIGS=($1)
    set +f
    local TESTS="$2"

    for cfg in ${KCONFIGS[@]}; do
        if ! check_kconfig "$cfg"; then
            skip_tests "${TESTS}"
        fi
    done
}

function test_pre_check {
    if [ -n "${FUNCTIONAL_LTP_HOMEDIR}" ] ; then
        # user or spec has specified a home directory for LTP.
        # check to see if runltp is installed there
        is_on_target runltp PROGRAM_RUNLTP $FUNCTIONAL_LTP_HOMEDIR

        # if we're not doing a build, then not having LTP installed
        # is a problem
        if [[ "$FUNCTIONAL_LTP_PHASES" != *build* ]] ; then
            assert_define PROGRAM_RUNLTP "Expected LTP to be present on board in $FUNCTIONAL_LTP_HOMEDIR, but it's not there!"
        fi
        LTP_DESTDIR=$FUNCTIONAL_LTP_HOMEDIR

        if [ -z "${FUNCTIONAL_LTP_PHASES}" ] ;  then
            # need to include deploy, because some Fuego-specific files
            # must be deployed, even if rest of LTP is already present
            FUNCTIONAL_LTP_PHASES="deploy run"
        fi
    else
        LTP_DESTDIR=$BOARD_TESTDIR/fuego.$TESTDIR

        if [ -z "${FUNCTIONAL_LTP_PHASES}" ] ;  then
            FUNCTIONAL_LTP_PHASES="build deploy run"
        fi
    fi

    assert_define SDKROOT
    assert_define CC
    assert_define AR
    assert_define RANLIB
    assert_define LDFLAGS
    assert_define FUNCTIONAL_LTP_TESTS

    # FIXTHIS: use regex for selecting tests to skip once merged on LTP upstream
    echo "Tests skipped by default in Fuego for now"
    echo "# skip these test cases" > ${LOGDIR}/skiplist.txt
    skip_tests "su01" # too complicated to setup
    skip_tests "sbrk03" # Only works in 32bit on s390 series system
    skip_tests "switch01" # Requires a 64-bit processor that supports little-endian mode,such as POWER6.
    skip_tests "ptrace04" # Requires blackfin processor
    skip_tests "cacheflush01" # Available only on MIPS-based systems
    skip_tests "bdflush01" # bdflush was deprecated by pdflush and pdflush removed from the kernel (https://lwn.net/Articles/508212/)
    skip_tests "fcntl06" # Linux kernel doesn't implement R_GETLK/R_SETLK
    skip_tests "fcntl06_64" # Linux kernel doesn't implement R_GETLK/R_SETLK
    skip_tests "munlockall02" # munlockall can be called form user space
    skip_tests "openat02" # Requires a filesystem with noatime and relatime
    skip_tests "utimensat01" # kernel patch f2b20f6ee842313a changed return value from -EACCESS to EPERM when a file is immutable and update time is NULL or UTIME_NOW

    echo "Tests skipped if the target /tmp folder is mounted on tmpfs"
    target_mounts=$(mktemp)
    if get "/proc/mounts" $target_mounts; then
        if grep "^tmpfs /tmp " $target_mounts; then
            echo "WARNING: /tmp is mounted using tmpfs."
            skip_tests "fallocate04"
        fi
    else
        echo "WARNING: could not check if /tmp is mounted using tmpfs"
    fi
    rm -f $target_mounts

    # Tests skipped by Linux kernel version
    # FIXTHIS: compare with the kernel version before adding tests to the skiplist
    kver=$(cmd "uname -r")
    echo "kernel version: $kver"
    skip_tests "adjtimex02" # needs a kernel that doesn't normalize buf.offset if it is outside the acceptable range
    skip_tests "fallocate04" # needs Linux 3.15 or newer
    skip_tests "add_key02" # needs a kernel with the 5649645d725c commit
    skip_tests "inotify06" # will loop/crash kernels that dont have commit 8f2f3eb59dff (<4.2)
    skip_tests "ptrace03" # Only works for <2.6.26, above that the kernel allows to trace init

    # Tests skipped depending on the availability of a command on the target
    # FIXTHIS: only check the necessary ones
    skip_if_command_unavailable bash "rwtest01 rwtest02 rwtest04 rwtest05 iogen01 fs_inod01 fs_di BindMounts"
    skip_if_command_unavailable expect "su01"
    skip_if_command_unavailable at "at_deny01 at_allow01"
    skip_if_command_unavailable cron "cron cron02 cron_deny01 cron_allow01 cron_dirs_checks01"
    skip_if_command_unavailable ar "ar"
    skip_if_command_unavailable ld "ld01"
    skip_if_command_unavailable ldd "ldd01"
    skip_if_command_unavailable nm "nm01"
    skip_if_command_unavailable file "file01"
    skip_if_command_unavailable tar "tar01"
    skip_if_command_unavailable logrotate "logrotate01"
    skip_if_command_unavailable mkfs.ext3 "ext4-nsec-timestamps"
    skip_if_command_unavailable mkfs.ext4 "ext4-nsec-timestamps"
    skip_if_command_unavailable tune2fs "ext4-nsec-timestamps"
    skip_if_command_unavailable mount "ext4-nsec-timestamps"
    skip_if_command_unavailable umount "ext4-nsec-timestamps"
    skip_if_command_unavailable touch "ext4-nsec-timestamps"
    skip_if_command_unavailable quotacheck "quotactl01"

    # Tests skipped depending on the configuration of the target kernel
    skip_if_kconfig_differs "CONFIG_INOTIFY_USER=y" "inotify_init1_01 inotify_init1_02 inotify01 inotify02 inotify03 inotify04 inotify05 inotify06"
    skip_if_kconfig_differs "CONFIG_FANOTIFY=y CONFIG_FANOTIFY_ACCESS_PERMISSIONS=y" "fanotify01 fanotify02 fanotify03 fanotify04 fanotify05 fanotify06"
    skip_if_kconfig_differs "CONFIG_EXT4_FS=y CONFIG_EXT4DEV_COMPAT=y CONFIG_EXT4_FS_XATTR=y CONFIG_EXT4_FS_POSIX_ACL=y CONFIG_EXT4_FS_SECURITY=y" "ext4-nsec-timestamps"
    skip_if_kconfig_differs "CONFIG_CHECKPOINT_RESTORE=y" "kcmp01 kcmp02 kcmp03"

    # Tests skipped depending on the architecture
    if [ "$ARCHITECTURE" = "x86_64" ]; then
        skip_tests "chown01_16 chown02_16 chown03_16 chown04_16 chown05_16"
        skip_tests "fchown01_16 fchown02_16 fchown03_16 fchown04_16 fchown05_16"
        skip_tests "getegid01_16 getegid02_16 geteuid01_16 geteuid02_16 getgid01_16 getgid03_16"
        skip_tests "getgroups01_16 getgroups03_16"
        skip_tests "getuid01_16 getuid03_16"
        skip_tests "lchown01_16 lchown02_16 lchown03_16"
        skip_tests "setfsgid01_16 setfsgid02_16 setfsgid03_16"
        skip_tests "setfsuid01_16 setfsuid02_16 setfsuid03_16 setfsuid04_16"
        skip_tests "setgid01_16 setgid02_16 setgid03_16"
        skip_tests "setgroups01_16 setgroups02_16 setgroups03_16 setgroups04_16"
        skip_tests "setregid01_16 setregid02_16 setregid03_16 setregid04_16"
        skip_tests "setresgid01_16 setresgid02_16 setresgid03_16 setresgid04_16"
        skip_tests "setresuid01_16 setresuid02_16 setresuid03_16 setresuid04_16 setresuid05_16"
        skip_tests "setreuid01_16 setreuid02_16 setreuid03_16 setreuid04_16 setreuid05_16 setreuid06_16 setreuid07_16"
        skip_tests "setuid01_16 setuid02_16 setuid03_16 setuid04_16"
        skip_tests "msgrcv08" # does not work on 64 bit
        skip_tests "readdir21" # This system call does not exist on x86-64 (man 2 readdir)
    fi

    if [ "$ARCHITECTURE" != "i386" ]; then
        skip_tests "modify_ldt01 modify_ldt02 modify_ldt03" # Only work on i386
    fi
}

function test_build {
    if [[ "$FUNCTIONAL_LTP_PHASES" == *build* ]] ; then
        echo "Building LTP"
        # Build the LTP tests
        make autotools
        ./configure CC="${CC}" AR="${AR}" RANLIB="${RANLIB}" LDFLAGS="$LDFLAGS" SYSROOT="${SDKROOT}" \
            --with-open-posix-testsuite --with-realtime-testsuite --without-perl --without-python --target=$PREFIX --host=$PREFIX \
            --prefix=`pwd`/target_bin --build=`uname -m`-unknown-linux-gnu

        # Skip tests depending on the results of configure (e.g.: SDK requirements)
        skip_if_config_unavailable "HAVE_XFS_QUOTA" "quotactl02" "Try adding xfslibs-dev to your SDK"
        skip_if_config_unavailable "HAVE_QUOTAV" "quotactl01 quotactl02" "Try adding libc6-dev to your SDK"
        skip_if_config_unavailable "HAVE_NUMA" "get_mempolicy01 mbind01 migrate_pages01 migrate_pages02 move_pages01 move_pages02 move_pages03 move_pages04 move_pages05 move_pages06 move_pages07 move_pages08 move_pages09 move_pages10 move_pages11" "No NUMA support"
        skip_if_config_unavailable "HAVE_MODIFY_LDT" "modify_ldt01 modify_ldt02 modify_ldt03" "Add modify_ldt support"

        # save build results in build.log
        # Typical build error:
        #   undefined reference to `io_submit'
        # Solution: install libaio for your arch
        #   E.g: /fuego-ro/toolchains/install_cross_toolchain.sh armh
        set -o pipefail
        make CC="${CC}" 2>&1 | tee build.log
        set +o pipefail

        make install

        cp --parents testcases/realtime/scripts/setenv.sh target_bin
        cp $TEST_HOME/ltp_target_run.sh target_bin
    else
        echo "Skipping LTP build"
        if [ -f "${WORKSPACE}/${JOB_BUILD_DIR}/buildskipfile.txt" ]; then
            cat "${WORKSPACE}/${JOB_BUILD_DIR}/buildskipfile.txt" >> ${LOGDIR}/skiplist.txt
        fi
    fi
}

function test_deploy {
    # Add tests to skip if "skiplist" is defined on the spec. The "skiplist"
    # can contain LTP test_case names (e.g.: inotify06) and/or absolute path(s)
    # to skipfiles (text files containing a list of LTP test case names)
    # usually located under /fuego-rw/boards/
    if [ -n "${FUNCTIONAL_LTP_SKIPLIST}" ]; then
        for item in "${FUNCTIONAL_LTP_SKIPLIST}"; do
            if [ -f "$item" ]; then
                cat "$item" >> ${LOGDIR}/skiplist.txt
            else
                echo "$item" >> ${LOGDIR}/skiplist.txt
            fi
        done
    fi

    if [[ "$FUNCTIONAL_LTP_PHASES" == *deploy* ]] && [[ -z "$PROGRAM_RUNLTP" ]] ; then
        echo "Deploying LTP"

        # make a staging area
        cp -r target_bin ltp

        # the syscalls group occupies by far the largest space so remove it if unneeded
        cp_syscalls=$(echo $FUNCTIONAL_LTP_TESTS | awk '{print match($0,"syscalls|quickhit|ltplite|stress.part")}')
        if [ $cp_syscalls -eq 0 ]; then
            echo "Removing syscalls binaries"
            awk '/^[^#]/ { print "rm -f ltp/testcases/bin/" $2 }' ltp/runtest/syscalls | sh
        fi

        cp ${LOGDIR}/skiplist.txt ltp

        put ltp/* $LTP_DESTDIR
        rm -rf ltp
    else
        echo "Skipping LTP deploy to board"
    fi

    if [[ "$FUNCTIONAL_LTP_PHASES" == *maketar* ]] ; then
        cp -r target_bin ltp
        echo "Creating LTP binary tarball"
        tar zcpf ltp.tar.gz ltp/
        mv ltp.tar.gz ${LOGDIR}/
        echo "LTP tar file ltp.tar.gz is available in ${LOGDIR}, for manual deployment"
    fi

    # pre-installed LTP needs a few fuego-specific items
    if [ -n "$FUNCTIONAL_LTP_HOMEDIR" ] ; then
       cmd "mkdir -p $FUNCTIONAL_LTP_HOMEDIR/testcases/realtime/scripts"
       put testcases/realtime/scripts/setenv.sh $FUNCTIONAL_LTP_HOMEDIR/testcases/realtime/scripts
       put $TEST_HOME/ltp_target_run.sh $FUNCTIONAL_LTP_HOMEDIR
       put ${LOGDIR}/skiplist.txt $FUNCTIONAL_LTP_HOMEDIR
    fi
}

function test_run {
    if [[ "$FUNCTIONAL_LTP_PHASES" == *run* ]] ; then
        echo "Running LTP"

        # we patch them here because of the different phases
        cmd "sed -i 's/^fork13 fork13 -i 1000000/fork13 fork13 -i 100000/' $LTP_DESTDIR/runtest/syscalls"
        cmd "sed -i 's/^dio30 diotest6 -b 65536 -n 100 -i 100 -o 1024000/dio30 diotest6 -b 65536 -n 5 -i 100 -o 1024000/' $LTP_DESTDIR/runtest/dio"
        cmd "sed -i 's/^msgctl11 msgctl11/msgctl11 msgctl11 -n 5/' $LTP_DESTDIR/runtest/syscalls"

        TESTS=""
        PTSTESTS=""
        RTTESTS=""

        # ltp_target_run.sh invokes the test different ways, depending
        # on the type of the test (regular, posize, or realtime)
        # Separate the test list by type, and pass the tests in different
        # variables
        for a in $FUNCTIONAL_LTP_TESTS; do
            for b in $ALLTESTS; do
                if [ "$a" == "$b" ]; then
                    TESTS+="$a "
                fi
            done

            for b in $ALLPTSTESTS; do
                if [ "$a" == "$b" ]; then
                    PTSTESTS+="$a "
                fi
            done

            for b in $ALLRTTESTS; do
                if [ "$a" == "$b" ]; then
                    RTTESTS+="$a "
                fi
            done
        done

        # Let some of the tests fail, the information will be in the result xlsx file
        report "cd $LTP_DESTDIR; TESTS=\"$TESTS\"; PTSTESTS=\"$PTSTESTS\"; RTTESTS=\"$RTTESTS\"; . ./ltp_target_run.sh"
    else
        echo "Skipping LTP run"

        # for a build-only test, put the build results into the testlog
        # (via roundtrip to board - pretty wasteful, but oh well...)
        put build.log $BOARD_TESTDIR/fuego.$TESTDIR
        report "cat $BOARD_TESTDIR/fuego.$TESTDIR/build.log"

        report_append "echo 'OK - LTP run skipped.'"
    fi
}

function test_fetch_results {
    if [[ "$FUNCTIONAL_LTP_PHASES" == *run* ]] ; then
        echo "Fetching LTP results"
        rm -rf result/
        get $LTP_DESTDIR/result $LOGDIR
    else
        echo "Skip fetching LTP run results"
    fi
}

function test_processing {
    if [[ "$FUNCTIONAL_LTP_PHASES" == *run* ]] ; then
        echo "Processing LTP results"
        SAVEDIR=$(pwd)
        cd ${LOGDIR}/result/
        # Restore the path so that python works properly
        export PATH=$ORIG_PATH
        cp $TEST_HOME/ltp_process.py .
        python ltp_process.py
        [ -e results.xlsx ] && cp results.xlsx ${LOGDIR}/results.xlsx
        [ -e rt.log ] && cp rt.log ${LOGDIR}
        cd ${SAVEDIR}
    else
        echo "Processing LTP build log"
        log_compare "$TESTDIR" "1836" "compile PASSED$" "p"
    fi
}
