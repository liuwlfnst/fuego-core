#!/usr/bin/python
import os, re, sys
sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib

regex_string = "^T:.*Min:\s+(\d+).*Avg:\s+(\d+) Max:\s+(\d+)"
measurements = {}
matches = plib.parse_log(regex_string)

if matches:
	min_latencies = []
	avg_latencies = []
	max_latencies = []
	for thread in matches:
		min_latencies.append(float(thread[0]))
		avg_latencies.append(float(thread[1]))
		max_latencies.append(float(thread[2]))
	measurements['default.latencies'] = [
		{"name": "max_latency", "measure" : max(max_latencies)},
		{"name": "min_latency", "measure" : min(min_latencies)},
		{"name": "avg_latency", "measure" : sum(avg_latencies)/len(avg_latencies)}]

sys.exit(plib.process(measurements))
