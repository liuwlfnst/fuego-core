FUNCTIONAL_KERNEL_BUILD_PER_JOB_BUILD="true"

function test_pre_check {
    echo "Doing a pre_check"
    # FIXTHIS: if making uImage, check for mkimage
}

function test_build {
    # Configuration
    if [ -z ${FUNCTIONAL_KERNEL_BUILD_ARCH+x} ]; then
        FUNCTIONAL_KERNEL_BUILD_ARCH="x86_64"
    fi

    if [ -z ${FUNCTIONAL_KERNEL_BUILD_CONFIG+x} ]; then
        FUNCTIONAL_KERNEL_BUILD_CONFIG="defconfig"
    fi
    echo "Configuring kernel with $FUNCTIONAL_KERNEL_BUILD_CONFIG"
    make ARCH=$FUNCTIONAL_KERNEL_BUILD_ARCH $FUNCTIONAL_KERNEL_BUILD_CONFIG

    # Building
    echo "Building Kernel"
    if [ -z ${FUNCTIONAL_KERNEL_BUILD_PARAMS+x} ]; then
        FUNCTIONAL_KERNEL_BUILD_PARAMS="-j4 bzImage modules"
    fi

    if [ ! -z ${FUNCTIONAL_KERNEL_BUILD_PLATFORM+x} ]; then
          OLD_PLATFORM=$PLATFORM
          PLATFORM=$FUNCTIONAL_KERNEL_BUILD_PLATFORM
          source $FUEGO_RO/toolchains/tools.sh
          PLATFORM=$OLD_PLATFORM
    fi

    make ARCH=$FUNCTIONAL_KERNEL_BUILD_ARCH $FUNCTIONAL_KERNEL_BUILD_PARAMS 2>&1 | tee build.log || true

    # weird bit here - put the build.log on the board... so that
    put build.log $BOARD_TESTDIR/fuego.$TESTDIR/build.log

    # cat on target populates the testlog with build.log
    report "cat $BOARD_TESTDIR/fuego.$TESTDIR/build.log"
}

function test_deploy {
    echo "Deploying kernel"
    # FIXTHIS: copy to tftp folder for lava etc..
}

function test_processing {
    echo "Processing kernel build log"
    if [ -z ${FUNCTIONAL_KERNEL_BUILD_REGEX_P+x} ]; then
        log_compare "$TESTDIR" "1" "^Kernel: arch/.* is ready" "p"
    else
        log_compare "$TESTDIR" "1" "$FUNCTIONAL_KERNEL_BUILD_REGEX_P" "p"
    fi
}


