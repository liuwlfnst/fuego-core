#!/usr/bin/python

import os, re, sys

sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib

ref_section_pat = "^\[[\w]+.[gle]{2}\]"
# groups           1        2
cur_search_str = "^(\w*):\ *([\d]{1,5}.[\d]{1,4}).*"

print "Reading current values from " + plib.TEST_LOG
cur_file = open(plib.TEST_LOG,'r')

lines = cur_file.readlines()
cur_file.close()

cur_dict = {}
for line in lines:
    m = re.match(cur_search_str, line)
    if m:
        name = m.group(1)
        value = m.group(2)
        cur_dict[name] = value

sys.exit(plib.process_data(ref_section_pat, cur_dict, 's', 'Rate, MB/s'))
