#!/usr/bin/python

import os, re, sys

sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib

#------------------------------------------------------------
#Client connecting to 10.90.101.49, TCP port 5001
#TCP window size: 16.0 KByte (default)
#------------------------------------------------------------
#[  3] local 10.90.100.60 port 38868 connected with 10.90.101.49 port 5001
#[ ID] Interval       Transfer     Bandwidth
#[  3]  0.0-15.0 sec   117 MBytes  65.4 Mbits/sec
#------------------------------------------------------------
#Server listening on TCP port 5001
#TCP window size: 85.3 KByte (default)
#------------------------------------------------------------
#------------------------------------------------------------
#Client connecting to 10.90.101.49, TCP port 5001
#TCP window size: 21.1 KByte (default)
#------------------------------------------------------------
#[  5] local 10.90.100.60 port 38869 connected with 10.90.101.49 port 5001
#[  4] local 10.90.100.60 port 5001 connected with 10.90.101.49 port 40772
#[ ID] Interval       Transfer     Bandwidth
#[  5]  0.0-15.0 sec  99.9 MBytes  55.7 Mbits/sec
#[  4]  0.0-15.2 sec  50.8 MBytes  28.0 Mbits/sec

# The following was also possible in the past for tx test:
#[  3]  0.0- 3.7 sec  9743717424271204 bits  0.00 (null)s/sec

ref_section_pat = "^\[[\w\d_ ./]+.[gle]{2}\]"
cur_search_pat = re.compile("^.* ([\d.]+) Mbits/sec\n.*\n.*\n.*\n.*\n.*\n.*\n.*\n.*\n.*\n.*\n.*\n.* ([\d.]+) Mbits/sec\n.* ([\d.]+) Mbits/sec", re.MULTILINE)

cur_dict = {}
pat_result = plib.parse(cur_search_pat)
if pat_result:
	for item in pat_result:
		#print item
		cur_dict["tcp.tx"] = item[0]
		cur_dict["tcp.bi_tx"] = item[1]
		cur_dict["tcp.bi_rx"] = item[2]

if "tcp.tx" in cur_dict:
	sys.exit(plib.process_data(ref_section_pat, cur_dict, 's', 'Rate, MB/s'))
else:
	print "Fuego error reason: could not parse measured bandwidth"
