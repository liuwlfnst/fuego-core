tarball=tiff-4.0.6.tar.gz

function test_pre_check {
    # This test requires 3 libraries to be present on the SDK
    # libtiff.so
    #    E.g.: apt-get install libtiff5-dev:armhf
    # liblzma.so
    #    E.g.: apt-get install liblzma-dev:armhf
    # libjpeg.so
    #    E.g.: apt-get install libjpeg-dev:armhf

    # NOTE: the use of /usr/lib/$PLATFORM-linux-*/ in the search path
    # is a bit of a stretch.  It is not often that the $PLATFORM variable
    # will have a sensible value to search.  However, it works in the
    # case of ARCH=arm64 and PLATFORM=aarch64.
    is_on_sdk libtiff.so LIBTIFF /lib:/usr/lib/:/usr/local/lib:/usr/lib/$ARCH-linux-*/:/usr/lib/$PLATFORM-linux-*/
    assert_define LIBTIFF "SDK is missing libtiff.so - cannot build test"
    LIBTIFF_PATH=$LOCATION

    is_on_sdk liblzma.so LIBLZMA /lib:/usr/lib/:/usr/local/lib:/usr/lib/$ARCH-linux-*/:/usr/lib/$PLATFORM-linux-*/
    assert_define LIBLZMA "SDK is missing liblzma.so - cannot build test"
    LIBLZMA_PATH=$LOCATION

    is_on_sdk libjpeg.so LIBJPEG /lib:/usr/lib/:/usr/local/lib:/usr/lib/$ARCH-linux-*/:/usr/lib/$PLATFORM-linux-*/
    assert_define LIBJPEG "SDK is missing libjpeg.so - cannot build test"
    LIBJPEG_PATH=$LOCATION
}

function test_build {
    echo " test_build tif.sh "
    $CC -O2 -pipe -g -I./libtiff  -Wall -W -Wl,-O1 -Wl,--hash-style=gnu -Wl,--as-needed -o  tools/.libs/tiff2bw tools/tiff2bw.c  $LIBTIFF_PATH $LIBLZMA_PATH $LIBJPEG_PATH -lz -lm -pthread
    $CC -O2 -pipe -g -I./libtiff  -Wall -W -Wl,-O1 -Wl,--hash-style=gnu -Wl,--as-needed -o  tools/.libs/tiff2pdf tools/tiff2pdf.c  $LIBTIFF_PATH $LIBLZMA_PATH $LIBJPEG_PATH -lz -lm -pthread
    $CC -O2 -pipe -g -I./libtiff  -Wall -W -Wl,-O1 -Wl,--hash-style=gnu -Wl,--as-needed -o  tools/.libs/tiff2ps tools/tiff2ps.c  $LIBTIFF_PATH $LIBLZMA_PATH $LIBJPEG_PATH -lz -lm -pthread
    $CC -O2 -pipe -g -I./libtiff  -Wall -W -Wl,-O1 -Wl,--hash-style=gnu -Wl,--as-needed -o  tools/.libs/tiff2rgba tools/tiff2rgba.c  $LIBTIFF_PATH $LIBLZMA_PATH $LIBJPEG_PATH -lz -lm -pthread
    $CC -O2 -pipe -g -I./libtiff  -Wall -W -Wl,-O1 -Wl,--hash-style=gnu -Wl,--as-needed -o  tools/.libs/tiffcmp tools/tiffcmp.c  $LIBTIFF_PATH $LIBLZMA_PATH $LIBJPEG_PATH -lz -lm -pthread
    $CC -O2 -pipe -g -I./libtiff  -Wall -W -Wl,-O1 -Wl,--hash-style=gnu -Wl,--as-needed -o  tools/.libs/tiffdither tools/tiffdither.c  $LIBTIFF_PATH $LIBLZMA_PATH $LIBJPEG_PATH -lz -lm -pthread
    $CC -O2 -pipe -g -I./libtiff  -Wall -W -Wl,-O1 -Wl,--hash-style=gnu -Wl,--as-needed -o  tools/.libs/tiffdump tools/tiffdump.c  $LIBTIFF_PATH $LIBLZMA_PATH $LIBJPEG_PATH -lz -lm -pthread
    $CC -O2 -pipe -g -I./libtiff  -Wall -W -Wl,-O1 -Wl,--hash-style=gnu -Wl,--as-needed -o  tools/.libs/tiffinfo tools/tiffinfo.c  $LIBTIFF_PATH $LIBLZMA_PATH $LIBJPEG_PATH -lz -lm -pthread
    $CC -O2 -pipe -g -I./libtiff  -Wall -W -Wl,-O1 -Wl,--hash-style=gnu -Wl,--as-needed -o  tools/.libs/tiffset tools/tiffset.c  $LIBTIFF_PATH $LIBLZMA_PATH $LIBJPEG_PATH -lz -lm -pthread
    $CC -O2 -pipe -g -I./libtiff  -Wall -W -Wl,-O1 -Wl,--hash-style=gnu -Wl,--as-needed -o  tools/.libs/tiffsplit tools/tiffsplit.c  $LIBTIFF_PATH $LIBLZMA_PATH $LIBJPEG_PATH -lz -lm -pthread
    $CC -O2 -pipe -g -I./libtiff  -Wall -W -Wl,-O1 -Wl,--hash-style=gnu -Wl,--as-needed -o  tools/.libs/tiffcp tools/tiffcp.c  $LIBTIFF_PATH $LIBLZMA_PATH $LIBJPEG_PATH -lz -lm -pthread
    $CC -O2 -pipe -g -I./libtiff  -Wall -W -Wl,-O1 -Wl,--hash-style=gnu -Wl,--as-needed -o  tools/.libs/tiffcrop tools/tiffcrop.c  $LIBTIFF_PATH $LIBLZMA_PATH $LIBJPEG_PATH -lz -lm -pthread
}

function test_deploy {
    put tools/1.tif tools/111.tif tools/11.tif tools/jello.tif $BOARD_TESTDIR/fuego.$TESTDIR/
    put tools/.libs/* $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; export PATH=$BOARD_TESTDIR/fuego.$TESTDIR:$PATH; chmod 777 *;\
        if tiff2bw 1.tif 2.tif; then echo 'TEST-1 OK'; else echo 'TEST-1 FAILED'; fi;\
        if tiff2pdf -o output.pdf 1.tif; then echo 'TEST-2 OK'; else echo 'TEST-2 FAILED'; fi;\
        if tiff2ps -1 jello.tif; then echo 'TEST-3 OK'; else echo 'TEST-3 FAILED'; fi;\
        if tiff2rgba -b jello.tif jello; then echo 'TEST-4 OK'; else echo 'TEST-4 FAILED'; fi;\
        if tiffcmp jello.tif 111.tif; then echo 'TEST-5 OK'; else echo 'TEST-5 FAILED'; fi;\
        if tiffdither -c lzw:2 111.tif 666.tif; then echo 'TEST-6 OK'; else echo 'TEST-6 FAILED'; fi;\
        if tiffdump 111.tif; then echo 'TEST-7 OK'; else echo 'TEST-7 FAILED'; fi;\
        if tiffinfo 111.tif; then echo 'TEST-8 OK'; else echo 'TEST-8 FAILED'; fi;\
        if tiffset jello.tif; then echo 'TEST-9 OK'; else echo 'TEST-9 FAILED'; fi;\
        if tiffsplit 11.tif -l; then echo 'TEST-10 OK'; else echo 'TEST-10 FAILED'; fi;\
        if tiffcp 11.tif 000.tif;then echo 'TEST-11 OK'; else echo 'TEST-11 FAILED'; fi;\
        if tiffcrop 11.tif 0000.tif;then echo 'TEST-12 OK'; else echo 'TEST-12 FAILED';fi"
}

function test_processing {
    log_compare "$TESTDIR" "12" "^TEST.*OK" "p"
    log_compare "$TESTDIR" "0" "^TEST.*FAILED" "n"
}
