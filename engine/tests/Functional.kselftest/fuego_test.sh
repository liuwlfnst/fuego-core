function test_pre_check {
    assert_define ARCHITECTURE
    if [ ! "$ARCHITECTURE" = "x86_64" ]; then
        assert_define CROSS_COMPILE
    fi
}

function test_build {
    make ARCH=$ARCHITECTURE defconfig
    make ARCH=$ARCHITECTURE headers_install
    make ARCH=$ARCHITECTURE -C tools/testing/selftests
}

function test_deploy {
    cd tools/testing/selftests
    mkdir fuego
    # kselftest targets (see tools/testing/selftests/Makefile)
    if [ ! -z ${FUNCTIONAL_KSELFTEST_TARGETS+x} ]; then
        INSTALL_PATH=fuego make TARGETS="$FUNCTIONAL_KSELFTEST_TARGETS" install || \
            abort_job "This test depends on tools/testing/selftests/Makefile having 'install' target support, which was introduced in kernel 4.1"
    else
        INSTALL_PATH=fuego make install || \
            abort_job "This test depends on tools/testing/selftests/Makefile having 'install' target support, which was introduced in kernel 4.1"
    fi
    put ./fuego/* $BOARD_TESTDIR/fuego.$TESTDIR/
    rm -rf fuego
    cd ../../../
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./run_kselftest.sh"
}

function test_processing {
    echo "Processing kselftest log"
    if [ ! -z ${FUNCTIONAL_KSELFTEST_FAIL_COUNT+x} ]; then
        log_compare "$TESTDIR" "$FUNCTIONAL_KSELFTEST_FAIL_COUNT" "^selftests: .* \[FAIL\]" "n"
    else
        log_compare "$TESTDIR" "0" "^selftests: .* \[FAIL\]" "n"
    fi
}
