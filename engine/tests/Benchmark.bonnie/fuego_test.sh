tarball=bonnie++-1.03e.tar.gz

function test_build {
    ./configure --host=$HOST --build=`uname -m`-linux-gnu;
    make
}

function test_deploy {
    put bonnie++  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    assert_define BENCHMARK_BONNIE_MOUNT_BLOCKDEV
    assert_define BENCHMARK_BONNIE_MOUNT_POINT
    assert_define BENCHMARK_BONNIE_SIZE
    assert_define BENCHMARK_BONNIE_ROOT

    if [ -z "$BENCHMARK_BONNIE_RAM" ] ; then
        BENCHMARK_BONNIE_RAM=0
    fi

    if [ -z "$BENCHMARK_BONNIE_NUM_FILES" ] ; then
        BENCHMARK_BONNIE_NUM_FILES="16:0:0:1"
    fi

    hd_test_mount_prepare $BENCHMARK_BONNIE_MOUNT_BLOCKDEV $BENCHMARK_BONNIE_MOUNT_POINT

    if [ "$BENCHMARK_BONNIE_ROOT" == "true" ]; then
        BONNIE_ROOT_PARAM="-u 0:0"
    else
        BONNIE_ROOT_PARAM=""
    fi

    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; pwd; ls; ./bonnie\+\+ -d $BENCHMARK_BONNIE_MOUNT_POINT/fuego.$TESTDIR $BONNIE_ROOT_PARAM -s $BENCHMARK_BONNIE_SIZE -r $BENCHMARK_BONNIE_RAM -n $BENCHMARK_BONNIE_NUM_FILES -m $NODE_NAME"

    sync

    hd_test_clean_umount $BENCHMARK_BONNIE_MOUNT_BLOCKDEV $BENCHMARK_BONNIE_MOUNT_POINT
}
