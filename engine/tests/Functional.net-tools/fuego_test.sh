function test_run {
    report "if arp; then echo 'TEST-1 OK'; else echo 'TEST-1 FAIL'; fi; \
    if hostname; then echo 'TEST-2 OK'; else echo 'TEST-2 FAIL'; fi; \
    if ifconfig; then echo 'TEST-3 OK'; else echo 'TEST-3 FAIL'; fi; \
    if ipmaddr; then echo 'TEST-4 OK'; else echo 'TEST-4 FAIL'; fi; \
    if iptunnel; then echo 'TEST-5 OK'; else echo 'TEST-5 FAIL'; fi; \
    if netstat; then echo 'TEST-6 OK'; else echo 'TEST-6 FAIL'; fi; \
    if route; then echo 'TEST-7 OK'; else echo 'TEST-7 FAIL'; fi"
}

function test_processing {
    P_CRIT="TEST.*OK"
    N_CRIT="TEST.*FAIL"

    log_compare "$TESTDIR" "7" "${P_CRIT}" "p"
    log_compare "$TESTDIR" "0" "${N_CRIT}" "n"
}
