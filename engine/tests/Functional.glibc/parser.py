#!/usr/bin/python
# See common.py for description of command-line arguments

import os, sys

sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib

measurements = {}

regex_string = '^TEST-(\d+) (.*)$'
matches = plib.parse_log(regex_string)

if matches:
    for m in matches:
        measurements['default.test' + m[0]] = 'PASS' if m[1] == 'OK' else 'FAIL'

sys.exit(plib.process(measurements))
